import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

Vue.use(VueRouter)
Vue.component('vehiculo-registro', require('pages/vehiculos/RegistroVehiculos.vue').default)
Vue.component('vehiculo-listado', require('pages/vehiculos/ListadoVehiculos.vue').default)
Vue.component('pesajes-informes', require('pages/pesaje/InformesPesajes.vue').default)
Vue.component('inicio-sesion', require('pages/login/InicioSesion.vue').default)
Vue.component('usuarios-listado', require('pages/usuarios/ListadoUsuarios.vue').default)
Vue.component('usuarios-registro', require('pages/usuarios/RegistroUsuarios.vue').default)
Vue.component('empresas-registro', require('pages/empresas/RegistroEmpresa.vue').default)
Vue.component('cargues-registro', require('pages/cargue/RegistroCargue.vue').default)
Vue.component('cargues-listado', require('pages/cargue/ListadoCargue.vue').default)
Vue.component('tarifas-registro', require('pages/tarifas/RegistroTarifas.vue').default)
Vue.component('tarifas-listado', require('pages/tarifas/ListadoTarifas.vue').default)
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */
export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,
    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.beforeEach((to, from, next) => {
    // console.log(permisos[2])
    // check if the route requires authentication and user is not logged in
    /* if (to.matched.some(routes => routes.meta.requiresAuth) && !store.getters['store/isLoggedIn']) {
      // redirect to login page
      next({ name: '/usuarios/login' })
      return
    } */

    // if logged in redirect to dashboard
    if (to.path === '/usuarios/login' && store.getters['store/isLoggedIn']) {
      next({ name: '/' })
      return
    }
    /* if (to.path === '/' && store.getters['store/isLoggedIn']) {
      next({ name: '/inicio/bienvenida' })
      return
    } */
    next()
  })

  return Router
}
