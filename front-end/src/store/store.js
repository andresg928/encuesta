import axios from 'axios'
const state = {
  url_api: 'http://181.143.216.37:9087',
  // url_api: 'http://localhost:8000',
  permisos: [],
  cliente_registrado: 0,
  movimiento_registro: 0,
  habilita_menu: 0,
  datos_editar_cliente: [],
  isLoggedIn: !!localStorage.getItem('token')
}
const mutations = {
  vehiculoIngresado (state) {
    state.movimiento_registro++
  },
  movimientoRegistro (state) {
    state.movimiento_registro++
  },
  habilitaMenu (state) {
    state.habilita_menu++
  },
  loginUser (state) {
    state.isLoggedIn = true
  },
  logoutUser (state) {
    state.isLoggedIn = false
  },
  obtenerPermisos (state, datos) {
    state.permisos = datos
  }
}
const actions = {
  vehiculoIngresado ({ commit }) {
    commit('vehiculoIngresado')
  },
  loginUser ({ commit }) {
    commit('loginUser')
  },
  logoutUser ({ commit }) {
    commit('logoutUser')
  },
  habilitaMenu ({ commit }) {
    commit('habilitaMenu')
  },
  movimientoRegistro ({ commit }) {
    commit('movimientoRegistro')
  },
  obtenerPermisos ({ commit }) {
    if (localStorage.getItem('token')) {
      axios.post(state.url_api + '/api/usuarios/obtener-usuario', {
        token: localStorage.getItem('token')
      }).then(response => {
        if (response.data.user.id) {
          // state.permisos = response.data.permisos
          commit('obtenerPermisos', response.data.permisos)
        } else {
          commit('obtenerPermisos', [])
        }
      }).catch(error => {
        console.log(error)
        commit('obtenerPermisos', [])
      })
    }
  }
}
const getters = {
  cliente_registrado: (state) => {
    return state.cliente_registrado
  },
  habilita_menu: (state) => {
    return state.habilita_menu
  },
  isLoggedIn: (state) => {
    return state.isLoggedIn
  },
  movimiento_registro: (state) => {
    return state.movimiento_registro
  },
  url_api: (state) => {
    return state.url_api
  },
  obtener_permisos: (state) => {
    return state.permisos
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
