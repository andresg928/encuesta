<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('api/encuesta/consulta-empleado/', 'EncuestaController@consultaInfoEmpleadosOfima');
Route::post('api/encuesta/registrar-encuesta/', 'EncuestaController@store');
Route::get('api/encuesta/exportr-datos/', 'EncuestaController@excelEncuestas');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
