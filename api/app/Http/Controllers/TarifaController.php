<?php

namespace App\Http\Controllers;

use App\Tarifa;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;

class TarifaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tarifas = Tarifa::select('id as id_tarifa', 'precio', 'deleted', 'iva_porcentaje', 'resolucion', 'fecha_inicio', 'fecha_final', 'prefijo',
                                  'rango_inicio', 'rango_final', 'tipo_producto'
                                 )
                        ->orderBy('created_at', 'DESC')
                        ->get();
        return $tarifas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $tarifa = Tarifa::find($id);
        //valido que solo exista una tarifa activa con el campo deleted se controla
        $tarifas = Tarifa::select('id as id_tarifa')->where('tipo_producto', $request->input('tipo'))->where('deleted', '0')->first();
        if(!$tarifas){
            $tarifa = new Tarifa();
            $tarifa->id = Uuid::generate()->string;
            $tarifa->precio = $request->input('precio');
            $tarifa->iva_porcentaje = $request->input('iva');
            $tarifa->resolucion = $request->input('resolucion');
            $tarifa->fecha_inicio = $request->input('fecha_inicio');
            $tarifa->fecha_final = $request->input('fecha_final');
            $tarifa->prefijo = $request->input('prefijo');
            $tarifa->rango_inicio = $request->input('rango_inicio');
            $tarifa->rango_final = $request->input('rango_final');
            $tarifa->id_usu_reg = JWTAuth::user()->id;
            $tarifa->tipo_producto = $request->input('tipo');
            $saved = $tarifa->save();
    
            if($saved === true){
                return response()->json([
                    "mensaje" => "registro_exitoso",
                    "registro" => $saved
                ],200);
            }
            else{
                return response()->json([
                    "mensaje" => "registro_no_exitoso",
                    "registro" => false
                ],500);                
            }
        } else {
            return response()->json([
                "mensaje" => "alerta",
                "registro" => false
            ],200);            
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tarifa  $tarifa
     * @return \Illuminate\Http\Response
     */
    public function show(Tarifa $tarifa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tarifa  $tarifa
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarifa $tarifa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tarifa  $tarifa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // $tarifas = Tarifa::select('id as id_tarifa')->where('deleted', '0')->first();
        $tarifa = Tarifa::find($request->id_tarifa);
        $tarifa->precio = $request->input('precio');
        $tarifa->iva_porcentaje = $request->input('iva');
        $tarifa->resolucion = $request->input('resolucion');
        $tarifa->fecha_inicio = $request->input('fecha_inicio');
        $tarifa->fecha_final = $request->input('fecha_final');
        $tarifa->prefijo = $request->input('prefijo');
        $tarifa->rango_inicio = $request->input('rango_inicio');
        $tarifa->rango_final = $request->input('rango_final');
        $tarifa->id_mod_reg = JWTAuth::user()->id;
        $tarifa->tipo_producto = $request->input('tipo');
        if($request->desactiva === false){
            $tarifa->deleted = '1';
        } else {
            $tarifa->deleted = '0';
        }
        $saved = $tarifa->save();

        if($saved === true){
            return response()->json([
                "mensaje" => "modificacion_exitosa",
                "modifico" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "modificacion_no_exitosa",
                "modifico" => false
            ],500);                
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tarifa  $tarifa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tarifa $tarifa)
    {
        //
    }
}
