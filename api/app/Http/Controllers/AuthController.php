<?php
namespace  App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use  JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class  AuthController extends  Controller {
    
	public  $loginAfterSignUp = true;

	public function listadoUsuarios(){
		$usuarios = User::select('users.id as id_usu', 'users.name', 'users.email', 'model_has_roles.role_id','roles.name as nom_rol')
		->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
		->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
		->orderBy('name', 'ASC')->get();
		return $usuarios;
	}

	public  function  register(Request  $request) {

		$user = new  User();
		$usuario_ = User::where('email', $request->email)->first();
		
        if (!$usuario_) {
			$user->name = $request->name;
			//$user->surname = $request->surname;
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			$saved = $user->save();

            if($saved === true){
				$user->assignRole($request->rol);
                return response()->json([
                    "mensaje" => "registro_exitoso",
                    "guardo" => $saved
                ],200);
            }
            else{
                return response()->json([
                    "mensaje" => "registro_no_exitoso"
                ],500);                
            }
			
			/*if ($this->loginAfterSignUp) {
				return  $this->login($request);
			}*/
	
			return  response()->json([
				'status' => 'ok',
				'data' => $user
			], 200);
		} else {
            return response()->json([
                "mensaje" => "existe_usuario"
            ],200);  
		}

	}

	public function actualizaUsuario(Request $request, $id){

		$usuario = User::find($id);
		$usuario->name = $request->name;
		$usuario->email = $request->email;
		$usuario->password = bcrypt($request->password);

		if($request->desactivar === true){
			$usuario->deleted = '1';
		}
		else {
			$usuario->deleted = '0';
		}

		$saved = $usuario->save();
		if($saved === true){
			$usuario->syncRoles([]);
			$usuario->assignRole($request->rol);
			return response()->json([
				"mensaje" => "actualizo_exitoso",
				"actualizo" => $saved
			],200);
		}
		else{
			return response()->json([
				"mensaje" => "modificacion_no_exitosa"
			],500);                
		}

	}

	public  function  login(Request  $request) {
		$input = $request->only('email', 'password');
		$jwt_token = null;
		if (!$jwt_token = JWTAuth::attempt($input)) {
			return  response()->json([
				'status' => 'invalid_credentials',
				'message' => 'Correo o contraseña no válidos.',
			], 200);
		}

		return  response()->json([
			'status' => 'ok',
			'token' => $jwt_token,
		]);
	}

	public  function  logout(Request  $request) {
		$this->validate($request, [
			'token' => 'required'
		]);

		try {
			JWTAuth::invalidate($request->token);
			return  response()->json([
				'status' => 'ok',
				'message' => 'Cierre de sesión exitoso.'
			]);
		} catch (JWTException  $exception) {
			return  response()->json([
				'status' => 'unknown_error',
				'message' => 'Al usuario no se le pudo cerrar la sesión.'
			], 500);
		}
	}

	public  function  getAuthUser(Request  $request) {
		$this->validate($request, [
			'token' => 'required'
		]);
		// $autorizo = JWTAuth::user()->hasRole('basculero');
		//$autorizo = JWTAuth::user()->hasPermissionTo('VEHICULO_MENU');
		$user = JWTAuth::authenticate($request->token);
		$permisos = JWTAuth::user()->getAllPermissions();
		return  response()->json(['user' => $user, 'permisos' => $permisos ]);
	}
}
