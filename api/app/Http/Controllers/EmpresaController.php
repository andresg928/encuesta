<?php

namespace App\Http\Controllers;

use App\Empresa;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use  JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::select('nit', 'nombre')->orderBy('nombre', 'ASC')->get();
        return $empresas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empresa_ = Empresa::where('nit', $request->input('nit'))->first();

        if(!$empresa_){
            $empresa = new Empresa();
            $empresa->id = Uuid::generate()->string;
            $empresa->nit = $request->input('nit');
            $empresa->nombre = $request->input('nombre');
            $empresa->id_usuario = JWTAuth::user()->id;
            $saved = $empresa->save();
    
            if($saved === true){
                return response()->json([
                    "mensaje" => "registro_exitoso",
                    "guardo" => $saved
                ],200);
            }
            else{
                return response()->json([
                    "mensaje" => "registro_no_exitoso"
                ],500);                
            }
        }
        else{
            return response()->json([
                "mensaje" => "existe_empresa"
            ],200);              
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show(Empresa $empresa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresa $empresa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empresa $empresa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresa $empresa)
    {
        //
    }
}
