<?php

namespace App\Http\Controllers;

use App\Cargue;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Vehiculo;
use App\Conductores;
use  JWTAuth;

class CargueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $cargues = Cargue::select(
                                    'cargues.id as id_cargue', 'cargues.placa as placa', 'cargues.peso_origen', 'empresas.nombre as empresa',
                                     'cargues.nit_empresa','cargues.estado', 'cargues.observaciones', 'conductores.documento', 'conductores.nombre'
                                  )
        ->leftjoin('empresas', 'cargues.nit_empresa', '=', 'empresas.nit')
        ->leftjoin('conductores', 'conductores.documento', '=', 'cargues.documento')
        //->whereBetween('pesajes.fecha', [$request->input('fecha_inicial'), $request->input('fecha_final')])
        //->where('tipo', 'like', '%' . $request->input('tipo_producto') . '%')
        ->orderBy('cargues.estado', 'ASC')
        ->orderBy('cargues.created_at', 'DESC')->get();

        return $cargues;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //REGISTRA EL CONDUCTOR
        if($request->input('coductor')  && $request->input('nombre_conductor') ){

            $conductor = Conductores::where('documento', $request->input('coductor'))->first();

            if(!$conductor) {
                $conduct = new Conductores();
                $conduct->id = Uuid::generate()->string;
                $conduct->documento = $request->input('coductor');
                $conduct->nombre = $request->input('nombre_conductor');
                $conduct->id_usuario = JWTAuth::user()->id;
                $conduct->save();
            }
        }

        //REGISTRA PLACA SI NO EXISTE
        $placa = Vehiculo::where('placa', $request->input('placa'))->first();

        if(!$placa) {
            $vehiculo = new Vehiculo();
            $vehiculo->id = Uuid::generate()->string;
            $vehiculo->placa = $request->input('placa');
            $vehiculo->deleted = '0';
            $vehiculo->save();
        }

        $cargue = new Cargue();
        $cargue->id = Uuid::generate()->string;
        $cargue->placa = $request->input('placa');
        $cargue->nit_empresa = $request->input('nit_empresa');
        $cargue->peso_origen = $request->input('peso_origen');
        $cargue->estado = 'activo';
        $cargue->id_usuario = JWTAuth::user()->id;
        $cargue->observaciones = $request->input('observaciones');
        $cargue->documento = $request->input('coductor');
        $saved = $cargue->save();

        if($saved === true){
            return response()->json([
                "mensaje" => "registro_exitoso",
                "id_cliente" => $cargue->placa,
                "guardo" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "registro_no_exitoso"
            ],500);                
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function show(Cargue $cargue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function edit(Cargue $cargue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_cargue)
    {
        //REGISTRA EL CONDUCTOR
        if($request->input('coductor')  && $request->input('nombre_conductor') ){

            $conductor = Conductores::where('documento', $request->input('coductor'))->first();

            if(!$conductor) {
                $conduct = new Conductores();
                $conduct->id = Uuid::generate()->string;
                $conduct->documento = $request->input('coductor');
                $conduct->nombre = $request->input('nombre_conductor');
                $conduct->id_usuario = JWTAuth::user()->id;
                $conduct->save();
            }
        }
        //REGISTRA PLACA SI NO EXISTE
        $placa = Vehiculo::where('placa', $request->input('placa'))->first();
        if(!$placa) {
            $vehiculo = new Vehiculo();
            $vehiculo->id = Uuid::generate()->string;
            $vehiculo->placa = $request->input('placa');
            $vehiculo->deleted = '0';
            $vehiculo->save();
        }
        $cargue = Cargue::find($id_cargue);
        $cargue->placa = $request->input('placa');
        $cargue->nit_empresa = $request->input('nit_empresa');
        $cargue->peso_origen = $request->input('peso_origen');
        //$cargue->estado = 'peso_2';
        $cargue->id_usuario_modifico = JWTAuth::user()->id;
        $cargue->observaciones = $request->input('observaciones');
        $cargue->documento = $request->input('coductor');
        $saved = $cargue->save();

        if($saved === true){
            return response()->json([
                "mensaje" => "registro_exitoso",
                "modifico" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "registro_no_exitoso"
            ],500);                
        }
    }
    public function inactivarCargue(Request $request, $id_cargue){
        $cargue = Cargue::find($id_cargue);
        $cargue->estado = 'inactivo';
        $cargue->transaccion_pesaje = $request->input('num_doc');
        $cargue->id_usuario_modifico = JWTAuth::user()->id;
        $cargue->save();
    }
    public function obtenerInfoCargue($placa){

        $cargue = Cargue::select('cargues.id as id_cargue', 'cargues.placa as placa', 'cargues.peso_origen', 'empresas.nombre as empresa', 'cargues.nit_empresa', 'cargues.documento', 'conductores.nombre as nom_conductor')
        ->where('estado', '=', 'activo')
        ->where('placa', '=', $placa)
        ->leftjoin('empresas', 'cargues.nit_empresa', '=', 'empresas.nit')
        ->leftjoin('conductores', 'cargues.documento', '=', 'conductores.documento')
        ->orderBy('cargues.created_at', 'DESC')->take(1)->get();

        return $cargue;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargue  $cargue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cargue $cargue)
    {
        //
    }
}
