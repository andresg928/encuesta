<?php

namespace App\Http\Controllers;

use App\EmpleadoExterno;
use Illuminate\Http\Request;

class EmpleadoExternoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmpleadoExterno  $empleadoExterno
     * @return \Illuminate\Http\Response
     */
    public function show(EmpleadoExterno $empleadoExterno)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmpleadoExterno  $empleadoExterno
     * @return \Illuminate\Http\Response
     */
    public function edit(EmpleadoExterno $empleadoExterno)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmpleadoExterno  $empleadoExterno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmpleadoExterno $empleadoExterno)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmpleadoExterno  $empleadoExterno
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmpleadoExterno $empleadoExterno)
    {
        //
    }
}
