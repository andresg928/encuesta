<?php

namespace App\Http\Controllers;

use App\Conductores;
use Illuminate\Http\Request;

class ConductoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conductores = Conductores::select('documento')->orderBy('documento', 'ASC')->get();
        return $conductores;
    }
    public function obtenerConductorPorDocumento(Request $request){

        $conductor = Conductores::select('documento', 'nombre')->where('documento', $request->documento)->first();
        return $conductor;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function show(Conductores $conductores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function edit(Conductores $conductores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conductores $conductores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conductores $conductores)
    {
        //
    }
}
