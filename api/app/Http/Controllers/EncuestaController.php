<?php

namespace App\Http\Controllers;

use App\Encuesta;
use App\EmpleadoExterno;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use App\Mail\NotificacionNovedadEncuesta;
use App\Exports\EncuestasExport;
use Maatwebsite\Excel\Facades\Excel;
use Mail;

class EncuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function asignarValoresPreguntas(){

        $preguntas = array(
            'opc_fiebre' => "opc_fiebre",
            'opc_garganta' => "opc_garganta",
            'opc_congestion_nasal' => "opc_congestion_nasal",
            'opc_tos' => "opc_tos",
            'opc_dificultad_respirar' => "opc_dificultad_respirar",
            'opc_fatiga' => "opc_fatiga",
            'opc_escalofrio' => "opc_escalofrio",
            'opc_dolor_musculo' => "opc_dolor_musculo",
            'opc_ninguno' => "opc_ninguno"
        );
        return $preguntas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $preguntas = $this->asignarValoresPreguntas();
        $respuestas = $request->input('respuestas');

        $encuesta = new Encuesta();
        $encuesta->id = Uuid::generate()->string;
        $encuesta->documento = $request->input('documento');
        $encuesta->temperatura = $request->input('temperatura');
        $novedad = 'si';
        if(count($respuestas) == 0) {
            $novedad = 'no';
            $respuestas = array( 0 => 'opc_ninguno');
        }
        
        for ($i=0;$i<count($respuestas);$i++){
                // se asigna las respuestas que vienen del formulario
                $campo = $respuestas[$i];
                $encuesta->$campo = '1';
                //elimino las respuestas para que en el array quede lo pendiente
                unset($preguntas[$respuestas[$i]]);
        }

        // se asigna lo faltante, o sea las no respondidas
        foreach($preguntas as $posicion=>$pregunta){
            $campo = $posicion;
            $encuesta->$campo = '0';
        }

        $encuesta->opc_estuve_contacto = $request->input('estuve');
        $encuesta->opc_otros_sintomas = $request->input('otros_sintomas');
        $encuesta->opc_convive = $request->input('convive');
        $saved = $encuesta->save();

        if($saved === true){
            //verifica si tuvo novedad
            if($novedad == 'si') {
                //obtener  el nombre del funcionario y pasarlo para el email notificado
                $info = $this->consultaInfoEmpleadosOfimaAux($encuesta->documento );
                $encuesta->nombre = $info[0]->nombre;
                $this->enviarNotificacion($encuesta);
            }

            return response()->json([
                "mensaje" => "registro_exitoso",
                "guardo" => $saved
            ],200);
        }
        else{
            return response()->json([
                "mensaje" => "registro_no_exitoso"
            ],500);                
        }
    }

    public function enviarNotificacion($datos = array()){

		$datos['email'] = 'a.leal@icoharinas.com.co';
		// $datos['tipo'] = 'usuario';

		Mail::to($datos['email'], $datos['nombre'])
			->send(new NotificacionNovedadEncuesta($datos));
	}

    /**
     * Display the specified resource.
     *
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function show(Encuesta $encuesta)
    {
        //
    }

    public function excelEncuestas(){   
        //dd($datos);   
        return Excel::download(new EncuestasExport(), 'Encuestas_Covid.xlsx');
    }

    public function consultaExternos($documento){
        $externo = EmpleadoExterno::select('documento as cedula', 'nombre as nombre')->where('documento', $documento)->where('deleted', '0')->get();
        return $externo;
    }

    // es auxiliar por que no pasa por el request
    public function consultaInfoEmpleadosOfimaAux ($documento) {

        $datos = \DB::connection('sqlsrv')
                                    ->table('MTEMPLEA as e')
                                    ->select('e.cedula', DB::raw("CONCAT(RTRIM(e.apellido), ' ', RTRIM(e.apellido2), ' ',RTRIM(e.nombre), ' ', RTRIM(e.nombre2) ) as nombre"), 'e.sexo', 'e.fecnac')
                                    ->where('e.cedula', '=', $documento)->get();

        // verifico si no esta en la tabla mtemplea, verifica en la tabla externos
        if(count($datos)<=0){
            $datos = $this->consultaExternos($documento);
        }
        return $datos;
    }

    public function consultaInfoEmpleadosOfima (Request $request) {

        $datos = \DB::connection('sqlsrv')
                                    ->table('MTEMPLEA as e')
                                    ->select('e.cedula', DB::raw("CONCAT(e.apellido, ' ', e.apellido2, ' ',e.nombre, ' ', e.nombre2 ) as nombre"), 'e.sexo', 'e.fecnac')
                                    ->where('e.cedula', '=', $request->documento)->get();

        // verifico si no esta en la tabla mtemplea, verifica en la tabla externos
        if(count($datos)<=0){
            $datos = $this->consultaExternos($request->documento);
        }
        return $datos;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function edit(Encuesta $encuesta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Encuesta $encuesta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Encuesta  $encuesta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Encuesta $encuesta)
    {
        //
    }
}
