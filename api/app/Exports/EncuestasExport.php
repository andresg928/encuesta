<?php

namespace App\Exports;
use App\Encuesta;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Http\Controllers\EncuestaController;

class EncuestasExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:N1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);
            },
        ];
    }
    public function headings(): array
    {
        return [
            'Fecha',
            'Documento',
            'Nombre',
            'Temperatura',
            'FIEBRE',
            'DOLOR DE GARGANTA',
            'CONGESTIÓN NASAL',
            'TOS',
            'DIFICULTAD PARA RESPIRAR',
            'FATIGA',
            'ESCALOFRÍO',
            'DOLOR DE MÚSCULOS',
            'NINGUNA DE LAS ANTERIORES',
            'OTROS SÍNTOMAS',
            '¿ESTUVO EN CONTACTO?',
            '¿CONVIVEN EN LA MISMA VIVIENDA?'
        ];
    }
    public function collection()
    {
        $encuestas = Encuesta::select(
            'created_at',
            'documento',
            //el alias debe tener el nombre del campo, en caso de que el campo no exista en la tabla
            'documento as nombre',
            'temperatura',
            'opc_fiebre',
            'opc_garganta',
            'opc_congestion_nasal',
            'opc_tos',
            'opc_dificultad_respirar',
            'opc_fatiga',
            'opc_escalofrio',
            'opc_dolor_musculo',
            'opc_ninguno',
            'opc_otros_sintomas',
            'opc_estuve_contacto',
            'opc_convive'
        )
        ->orderBy('created_at', 'DESC')->get();
        //recorro la respuesta para añadir el nombre de la persona que no pertenece al modelo
        $e = new EncuestaController();
        foreach ($encuestas as $key => $encuesta) {
            $result = $e->consultaInfoEmpleadosOfimaAux($encuestas[$key]['documento']);
            //el nombre debe ser igual al de select para que quede en la misma posicion
            $encuestas[$key]['nombre'] = $result[0]->nombre;
          }
       
        return $encuestas;

    }

}
