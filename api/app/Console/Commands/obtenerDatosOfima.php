<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\FacturacionController; 

class obtenerDatosOfima extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minute:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando obtiene la informacion cada minuto en ofima y envia a la tabla facturacion de app_bascula';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $facturacion_ctrl = new FacturacionController();
        $facturacion_ctrl->insertarDatosOfimaFacturacion();
    }
}
