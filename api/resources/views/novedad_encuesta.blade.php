<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title></title>
</head>
<body>

<table border="0" width="100%" style="width:100%">
    <tr>
        <td width="10%"><b>Funcionario</b></td>
        <td width="90%" align="left">{{$datos['nombre']}}</td>
    </tr>
    <tr>
        <td width="10%"><b>Temperatura</b></td>
        <td width="90%" align="left">{{$datos['temperatura']}}</td>
    </tr>
    @if( $datos['opc_otros_sintomas'] != '' )
    <tr>
        <td width="10%"><b>Otros síntomas</b></td>
        <td width="90%" align="left">{{$datos['opc_otros_sintomas']}}</td>
    </tr>
    @endif
    @if( $datos['opc_convive'] == 's' )
    <tr>
        <td width="10%"><b>Comparte la misma vivienda</b></td>
        <td width="90%" align="left">SI</td>
    </tr>
    @endif
    @if( $datos['opc_convive'] == 'n' )
    <tr>
        <td width="10%"><b>Comparte la misma vivienda</b></td>
        <td width="90%" align="left">NO</td>
    </tr>
    @endif
    <tr>
        <td colspan="2">
            <table border="1" style="border-collapse: collapse;">
                <tr class="encabezado">
                    <td><b>FIEBRE</b></td>
                    <td><b>DOLOR DE GARGANTA</b></td>
                    <td><b>CONGESTIÓN NASAL</b></td>
                    <td><b>TOS</b></td>
                    <td><b>DIFICULTAD PARA RESPIRAR</b></td>
                    <td><b>FATIGA</b></td>
                    <td><b>ESCALOFRÍO</b></td>
                    <td><b>DOLOR DE MÚSCULOS</b></td>
                    <td><b>¿ESTUVO CONTACTO?</b></td>
                </tr>
                <tr>
                    @if( $datos['opc_fiebre'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_garganta'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_congestion_nasal'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_tos'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_dificultad_respirar'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_fatiga'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_escalofrio'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_dolor_musculo'] == '1' )
                        <td align="center" bgcolor="#ff0000">X</td>
                    @else
                        <td align="center"></td>
                    @endif

                    @if( $datos['opc_estuve_contacto'] == 's' )
                        <td align="center">SI</td>
                    @else
                        <td align="center">NO</td>
                    @endif
                </tr>
            </table>
        </td>
    </tr>
</table>

 </body>
</html>

<style>
    html { margin: 3px;font-family: Arial, Helvetica, sans-serif;}
    .titulos {
      font-size:10px;
      font-weight: bold; 
    }
    .titulos2 {
      font-size:14px;
      font-weight: bold; 
    }
    .letra_normal {
      font-size:12px;
    }
    .encabezado {
        border: 1px;
    }
</style>