<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['id' => 6,'name' => 'VEHICULO_REGISTRO', 'guard_name' => 'web']);
        Permission::create(['id' => 7,'name' => 'VEHICULO_MENU', 'guard_name' => 'web']);
        Permission::create(['id' => 8,'name' => 'VEHICULO_EDITAR', 'guard_name' => 'web']);
        Permission::create(['id' => 9,'name' => 'CARGUE_MENU', 'guard_name' => 'web']);
        Permission::create(['id' => 10,'name' => 'CARGUE_REGISTRO', 'guard_name' => 'web']);
        Permission::create(['id' => 11,'name' => 'EMPRESA_REGISTRO', 'guard_name' => 'web']);
        Permission::create(['id' => 12,'name' => 'PESAJE_TIPO_PRODUCTO_TRIGO', 'guard_name' => 'web']);
        Permission::create(['id' => 13,'name' => 'INFORMES_MENU', 'guard_name' => 'web']);
        Permission::create(['id' => 14,'name' => 'PESAJE_REGISTRO_INICIAL', 'guard_name' => 'web']);
        Permission::create(['id' => 15,'name' => 'INFORMES_EXPORTAR_EXCEL', 'guard_name' => 'web']);
        Permission::create(['id' => 16,'name' => 'INFORMES_TIPO_PRODUCTO', 'guard_name' => 'web']);
        Permission::create(['id' => 17,'name' => 'INFORMES_REIMPRIMIR_TICKET', 'guard_name' => 'web']);
        Permission::create(['id' => 18,'name' => 'USUARIO_MENU', 'guard_name' => 'web']);
        Permission::create(['id' => 19,'name' => 'USUARIO_REGISTRO', 'guard_name' => 'web']);
        Permission::create(['id' => 20,'name' => 'USUARIO_EDITAR', 'guard_name' => 'web']);
        Permission::create(['id' => 21,'name' => 'USUARIO_TAG_ADM_USU', 'guard_name' => 'web']);
        Permission::create(['id' => 22,'name' => 'USUARIO_TAG_ROL_PERMISO', 'guard_name' => 'web']);
        Permission::create(['id' => 23,'name' => 'CARGUE TRIGO', 'guard_name' => 'web']);
        Permission::create(['id' => 24,'name' => 'USUARIO_REGISTRO_ROL_PERMISO', 'guard_name' => 'web']);
        Permission::create(['id' => 25,'name' => 'USUARIO_EDITA_ROL_PERMISO', 'guard_name' => 'web']);
        Permission::create(['id' => 26,'name' => 'USUARIO_ASIGNA_PERMISOS', 'guard_name' => 'web']);
        Permission::create(['id' => 27,'name' => 'PESAJE_MENU', 'guard_name' => 'web']);
        Permission::create(['id' => 28,'name' => 'CARGUE_EDITAR', 'guard_name' => 'web']);
        Permission::create(['id' => 29,'name' => 'PESAJE_REGISTRO_FINAL', 'guard_name' => 'web']);
        Permission::create(['id' => 30,'name' => 'PESAJE_TIPO_PRODUCTO_TERMINADO', 'guard_name' => 'web']);
    }
}
