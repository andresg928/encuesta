<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encuestas', function (Blueprint $table) {
            $table->uuid('id')->unique()->index();
            $table->string('documento',20);
            $table->string('temperatura',4);
            $table->string('opc_fiebre',1);
            $table->string('opc_garganta',1);
            $table->string('opc_congestion_nasal',1);
            $table->string('opc_fatiga',1);
            $table->string('opc_tos',1);
            $table->string('opc_dificultad_respirar',1);
            $table->string('opc_escalofrio',1);
            $table->string('opc_dolor_musculo',1);
            $table->string('opc_ninguno',1);
            $table->string('opc_otros_sintomas',100);
            $table->string('opc_convive',1);
            $table->string('opc_estuve_contacto',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encuestas');
    }
}
